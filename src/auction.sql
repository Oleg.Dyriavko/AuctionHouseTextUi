
use auction;

CREATE TABLE seller (
seller_id INT(11) AUTO_INCREMENT, 
NAME VARCHAR(50),
PRIMARY KEY (seller_id)
);

INSERT INTO seller(NAME) VALUES("Gosha");

INSERT INTO seller(NAME) VALUES("Robert");

INSERT INTO seller(NAME) VALUES("Penelopa");


CREATE TABLE product (
product_id INT(11) AUTO_INCREMENT, 
city VARCHAR(100), 
street VARCHAR(50), 
NUMBER INT(11),
seller_id INT(11),
PRIMARY KEY (product_id),
CONSTRAINT FK_product_seller FOREIGN KEY (seller_id)
REFERENCES seller(seller_id));

INSERT INTO product(product_id, city, street, NUMBER, seller_id) VALUES("Dnepr", "Shevchenko", 20, 1);

INSERT INTO product(product_id, city, street, NUMBER, seller_id) VALUES("Kiev", "Pushkina", 65, 2);

INSERT INTO product(product_id, city, street, NUMBER, seller_id) VALUES("Odessa", "Deribasovskaya", 15, 3);

CREATE TABLE buyer (
buyer_id INT(11) AUTO_INCREMENT, 
first_name VARCHAR(50), 
last_name VARCHAR(50), 
PRIMARY KEY (buyer_id));

INSERT INTO buyer (buyer_id, first_name, last_name) VALUES("Oleg", "Titov");

INSERT INTO buyer (buyer_id, first_name, last_name) VALUES("Ivan", "Semenyuk");

INSERT INTO buyer (buyer_id, first_name, last_name) VALUES("Julia", "Bereta");

CREATE TABLE bid (
bid_id INT(11) AUTO_INCREMENT, 
buyer_id INT(11), 
product_id INT(11), 
nominal INT(11),
PRIMARY KEY (bid_id),
CONSTRAINT FK_bid_product FOREIGN KEY (product_id)
REFERENCES product(product_id),
CONSTRAINT FK_bid_buyer FOREIGN KEY (buyer_id)
REFERENCES buyer(buyer_id));

INSERT INTO bid (buyer_id, product_id, nominal) VALUES(2, 125);

INSERT INTO bid (bid_id, buyer_id, product_id, nominal) VALUES(2, 2, 500);

INSERT INTO bid (bid_id, buyer_id, product_id, nominal) VALUES(3, 3, 300);

INSERT INTO bid (buyer_id, product_id, nominal) VALUES(1, 8, 78);


SELECT * FROM seller;
SELECT * FROM product;
SELECT * FROM buyer;
SELECT * FROM bid;

SET SQL_SAFE_UPDATES = 0;
UPDATE product SET NUMBER = 99 WHERE city = 'Dnepr';
SET SQL_SAFE_UPDATES = 1;


SELECT *
 FROM bid b 
INNER JOIN product p
ON b.product_id = p.product_id
INNER JOIN buyer bu
ON bu.buyer_id = b.buyer_id
WHERE p.product_id = 8;









