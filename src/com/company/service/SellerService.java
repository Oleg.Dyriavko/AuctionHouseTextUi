package com.company.service;

import com.company.domain.Seller;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SellerService {


    public void saveSeller(String name) throws SQLException {
        String insertSeller = "INSERT INTO seller (name) VALUES(?)";
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/auction",
                "root",
                "root");
             PreparedStatement insertStatement = connection.prepareStatement(insertSeller)) {
            insertStatement.setString(1, name);
            insertStatement.executeUpdate();
        }
    }

    public List<Seller> getAllSellers() throws SQLException {
        String allSellers = "SELECT * FROM seller";
        List<Seller> sellers = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/auction",
                "root",
                "root");
        ) {
            ResultSet resultSet = connection.createStatement().executeQuery(allSellers);
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                int sellerId = resultSet.getInt("seller_id");
                Seller seller = new Seller(sellerId, name);
                sellers.add(seller);
            }
        }

        return sellers;
    }

}
