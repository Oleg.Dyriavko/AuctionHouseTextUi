package com.company.service;

import com.company.domain.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductService {

    public static final String INSERT_PRODUCT = "INSERT INTO product (city, street, number, seller_id) " +
            "VALUES(?, ?, ?, ?)";

    public void saveProduct(String city, String street, int number, int sellerId) throws SQLException {
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/auction",
                "root",
                "root");
             PreparedStatement insertStatement = connection.prepareStatement(INSERT_PRODUCT)) {
            insertStatement.setString(1, city);
            insertStatement.setString(2, street);
            insertStatement.setInt(3, number);
            insertStatement.setInt(4, sellerId);
            insertStatement.executeUpdate();
        }
    }

    public List<Product> getAllProducts() throws SQLException {
        String allProducts = "SELECT * FROM product";
        List<Product> products = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/auction",
                "root",
                "root");
        ) {
            ResultSet resultSet = connection.createStatement().executeQuery(allProducts);
            while (resultSet.next()) {
                int productId = resultSet.getInt("product_id");
                String city = resultSet.getString("city");
                String street = resultSet.getString("street");
                int number = resultSet.getInt("number");
                int seller_id = resultSet.getInt("seller_id");

                Product product = new Product(productId, city, street, number, seller_id);
                products.add(product);
            }
        }

        return products;
    }

}