package com.company.domain;

public class Bid {
    private int id;
    private int buyerId;
    private int productId;
    private int nominal;

    public Bid(int id, int buyerId, int productId, int nominal) {
        this(buyerId, productId, nominal);
        this.id = id;
    }

 public Bid(int buyerId, int productId, int nominal) {
        this.buyerId = buyerId;
        this.productId = productId;
        this.nominal = nominal;
    }

    public Bid(int nominal) {
        this.nominal = nominal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getNominal() {
        return nominal;
    }

    public void setNominal(int nominal) {
        this.nominal = nominal;
    }

    @Override
    public String toString() {
        return "Bid{" +
                "id=" + id +
                ", buyerId=" + buyerId +
                ", productId=" + productId +
                ", nominal=" + nominal +
                '}';
    }
}
