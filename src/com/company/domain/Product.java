package com.company.domain;

public class Product {
    private int id;
    private String city;
    private String street;
    private int number;
    private int sellerId;


    public Product(int id, String city, String street, int number, int sellerId) {
        this(city, street, number, sellerId);
        this.id = id;
    }

    public Product(String city, String street, int number, int sellerId) {
        this.city = city;
        this.street = street;
        this.number = number;
        this.sellerId = sellerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", number=" + number +
                ", sellerId=" + sellerId +
                '}';
    }
}
